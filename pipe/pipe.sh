#!/usr/bin/env bash
#
# Enables a feature flag in LaunchDarkly, https://launchdarkly.com/
#
# Required globals:
#

source "$(dirname "$0")/common.sh"

info "Executing the pipe..."

## Enable debug mode.
enable_debug() {
  if [[ "${DEBUG}" == "true" ]]; then
    info "Enabling debug mode."
    set -x
  fi
}

validate() {
  # required parameters
  NAME=${NAME:?'NAME variable missing.'}

  # default parameters
  GREETING=${GREETING:="Hello world"}
  DEBUG=${DEBUG:="false"}
}

run_pipe() {
  run echo "${GREETING} ${NAME}"
  if [[ "${status}" == "0" ]]; then
    echo "${output}" > pipe.output
    success "Success!"
  else
    fail "Error!"
    exit 1
  fi

}

enable_debug
validate
run_pipe
