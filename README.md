# Bitbucket Pipelines Pipe: My demo pipe

This pipe is an example to show how easy is to create pipes for Bitbucket Pipelines.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: rgomish/demo-pipe:0.2.1
    variables:
      NAME: "<string>"
      # GREETING: "<string>" # Optional.
      # DEBUG: "<boolean>" # Optional.
      
```
## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| NAME (*)              | The name that will be printed in the logs |
| GREETING              | The optional alternative greeting. |
| DEBUG                 | Turn on extra debug information. Default: `false`. |

_(*) = required variable._

## Details
This pipe demonstrates a simple pipe. You can use this pipe as a basis to create your own pipe.

## Prerequisites

## Examples

Basic example:

```yaml
script:
  - pipe: rgomish/demo-pipe:0.2.1
    variables:
      NAME: "foobar"
```

Advanced example:

```yaml
script:
  - pipe: rgomish/demo-pipe:0.2.1
    variables:
      NAME: "foobar"
      GREETING: "Good morning"
```

## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on community][community].

If you’re reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce


## License
Copyright (c) 2018 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.

[community]: https://community.atlassian.com/t5/forums/postpage/choose-node/true/interaction-style/qanda?add-tags=bitbucket-pipelines,pipes,atlassian,demo
